#define _TASK_TIMECRITICAL

// #define INDIRECT_TEMPERATURE

#include <Adafruit_MAX31855.h>
#include <Arduino.h>
#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

// The atomic update has been tested, but this is still nice to have
#ifndef ATOMIC_FS_UPDATE
#include "Emergency_mode.h"
#else
#warning("Atomic update has been enabled, emergency mode not needed")
#endif

#include <FIR.h>
#include "FIRFilter.h"
#include <Hash.h>
#include <HomeEnvironment.h>
#include <LittleFS.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>
#include <SPI.h>
#include <StreamUtils.h>
#include <TaskScheduler.h>
#include <vector>
#include "../include/Version.h"
#include <Wire.h>
#include <Updater.h>
#include "logs.h"

extern "C"
{
#include <ping.h>
}

// Define the log macro's here
#define ESP_LOGE(TAG, ...) logerror(__VA_ARGS__)
#define ESP_LOGW(TAG, ...) logwarn(__VA_ARGS__)
#define ESP_LOGI(TAG, ...) loginfo(__VA_ARGS__)
#define ESP_LOGD(TAG, ...) logdebug(__VA_ARGS__)
#define ESP_LOGV(TAG, ...) logtrace(__VA_ARGS__)

#define MHz(x) (x * 1000000)
#define kHz(x) (x * 1000)

#define EEPROM_PID_ADDR 0x0
#define EEPROM_PID_MAGIC_VAL 145

#ifndef ATOMIC_FS_UPDATE
#define RTC_MEMORY_UPDATE_SIZE 65
#endif

#define SAFETIES_ENABLED 0

#define TEMPERATURE_UPDATE_INTERVAL 200 // ms, conversion time for the MAX6675 is typ: 0.17 sec to 0.22 sec

#define SSR_PIN D0
#define NTC_CLK D5
#define NTC_DAT D6
#define NTC_1_CS D1
#define NTC_2_CS D2

#define PID_MIN_WINDOW_SIZE 0 //ms, used to keep the heater alive
#define PID_WINDOW_SIZE 2000  //ms

#define FRAME_HISTORY_SIZE (50 + 1) // 1000 frames history + 1 current frame
#define FRAME_INTERVAL 1000         //ms

#define MAX_JSON_SIZE 3000

#define MAX_SET_POINTS 200

#define PREHEAT_TIME 60000 //ms, or 60 seconds

#define PID_CONFIG "/pid.json"

#define TAG "YARO"
#define STORED_PROFILES_PATH "/pro"

namespace MemberNames
{
    const String kProfileName = "prof_name";
    const String kProfileDuration = "prof_dur";
    const String kSetPoints = "sp";
    const String kSetPointCount = "sp_cnt";
    const String kSetPointDuration = "d";
    const String kSetPointTemperature = "t";
    const String kTemperature = kSetPointTemperature;
    const String kError = "error";
} // namespace MemberNames

///////////////////////////////////
//          Data structures      //
///////////////////////////////////

typedef struct SetPoint
{
    double temperature; // in celcius
    double duration;    // in seconds
    void *prev;         // previous setpoint
    void *next;         // next setpoint
} SetPoint_t;

typedef struct Profile
{
    SetPoint_t *setPoints; // The array of setpoints
    size_t setPointCount;  // The number of setpoints, must not be bigger than MAX_SET_POINTS
    String name;           // The name of the profile
    double totalDuration;  // Get's accumulated automatically
} Profile_t;

typedef struct DataFrame
{
    double NTC1, NTC2;           // The NTC temperatures and their delta's compared to the previous frame
    time_t time;                 // in seconds
    SetPoint_t *currentSetPoint; // the current setpoint
    bool valid = false;          // is this frame valid?
    void *prev;

    DataFrame()
    {
        this->valid = false;
    }

    DataFrame(const DataFrame &in)
    {
        this->NTC1 = in.NTC1;
        this->NTC2 = in.NTC2;
        this->time = in.time;
        this->currentSetPoint = in.currentSetPoint;
        this->valid = in.valid;
        this->prev = in.prev;
    }

    void reset()
    {
        this->NTC1 = this->NTC2 = 0.0;
        this->time = 0;
        this->currentSetPoint = nullptr;
        this->prev = nullptr;
        this->valid = false;
    }
} DataFrame_t;

typedef enum State
{
    ERROR = -1,
    IDLE = 0,
    ACTIVE,
    FINISHED,
    STOPPED,
    AUTOTUNING
} State_t;

typedef enum HeatingStage
{
    WAITING = 0,
    PREHEAT,
    PROFILE,
    COOLDOWN,
    AUTOTUNE
} HeatingStage_t;

///////////////////////////////////
//       End data structures     //
///////////////////////////////////

///////////////////////////////////
//      Data frame variables     //
///////////////////////////////////

long dataFrameTimestamp = 0;
DataFrame_t currentFrame;
std::vector<DataFrame_t> frameHistory(FRAME_HISTORY_SIZE);

///////////////////////////////////
//   End data frame variables    //
///////////////////////////////////

///////////////////////////////////
//       Profile variables       //
///////////////////////////////////

Profile_t *activeProfile;
const SetPoint_t idleSetPoint{.temperature = 15, .duration = 0};
const SetPoint_t autoTuneSetPoint{.temperature = 80, .duration = 3600, .prev = nullptr, .next = nullptr};
///////////////////////////////////
//      End profile variables    //
///////////////////////////////////

///////////////////////////////////
//      Execution variables      //
///////////////////////////////////

State_t currentState = State::IDLE;
HeatingStage_t currentHeatingStage = HeatingStage::WAITING;

time_t startTime;
time_t stopTime;
time_t preheatStart;

double preheatStartTemp;

String errorMessage;

long outputMillis = 0;
long outputWindow = 0;

bool shouldReboot = false;
bool fsUpdate = false;
bool updateStarted = false;

///////////////////////////////////
//    End Execution variables    //
///////////////////////////////////

///////////////////////////////////
//          PID variables        //
///////////////////////////////////

// TODO the last profile needs a much quicker response
// The last set of tuning parameters are using the Proportional on measure
// http://brettbeauregard.com/blog/2017/06/introducing-proportional-on-measurement/
typedef struct PID_Tunings
{
    uint8_t magicValue = EEPROM_PID_MAGIC_VAL;
    double pValues[3] = {0.2, 200, 160};
    double iValues[3] = {0.2, 6.8, 1.2};
    double dValues[3] = {120, 15, 5};
} PID_Tunings_t;

PID_Tunings_t pidTunings;
uint8_t pidTuning = 0;
double pidIn, pidOut, pidSetPoint;

// PID Autotuning variables
byte ATuneModeRemember = 2;
bool autoTuning = false;
double aTuneStep = 50, aTuneNoise = 1, aTuneStartValue = 100;
unsigned int aTuneLookBack = 200;

// This PID uses the averaged values of the two NTC's, create two PID objects for top/bottom control
PID pid(&pidIn, &pidOut, &pidSetPoint, pidTunings.pValues[0], pidTunings.iValues[0], pidTunings.dValues[0], P_ON_M, DIRECT);
PID_ATune aTune(&pidIn, &pidOut);
///////////////////////////////////
//       End PID variables       //
///////////////////////////////////

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
HomeEnvironment environment(defined_wifi_ssid, defined_wifi_password, "Yet Another Reflow Oven", defined_ota_password);

Adafruit_MAX31855 ntc1(NTC_CLK, NTC_1_CS, NTC_DAT); //chip select pin
Adafruit_MAX31855 ntc2(NTC_CLK, NTC_2_CS, NTC_DAT); //chip select pin

// http://t-filter.engineerjs.com/
FIR<double, FILTER_TAP_NUM> firFilter1;
FIR<double, FILTER_TAP_NUM> firFilter2;

///////////////////////////////////
//     Function declarations     //
///////////////////////////////////

void output();
void process();
void wifiLoop();
void safetyLoop();

bool shouldPreheat();
bool makeDataFrame();
bool checkSafeties();
void storeDataFrame();
void readTemperatures();
void moveFramesAndcalculateDeltas();

bool stop();
bool start();
bool reset();
bool finish();

// PID Things
void setPidTuning(uint8_t tuningIndex);
void updateAutotune();

void merge_json(JsonVariant dst, JsonVariantConst src);

// State functions
bool setState(const State_t &state);

// Profile functions
bool loadProfile(String profileName);
bool deleteProfile(String profileName);
bool getFileInfo(File file, JsonObject &output);
bool downloadProfile(JsonVariant &root, const String &profileName);

String getProfileFileName(const String &profileName);

double getProfileTimeElapsed();              // Returns the elapsed time of the current runningprofile
double getProfileTimeRemaining();            // Returns the remaining time of the current runningprofile
double getTimeElapsedForCurrentSetPoint();   // Calculates the elapsed time for the current setpoint
double getTimeRemainingForCurrentSetPoint(); // Calculates the remaining time for the current setpoint

// This function returns the temperature value that is used to smooth out the curve.
// This makes sure that the value increases according to the profile curve.
double getSetPointValue();

// Setpoint functions
SetPoint_t *getCurrentSetPoint();                     // Returns the expected setpoint based on the elapsed time
SetPoint_t *getSetPointFromTime(time_t time);         // Returns the expected setpoint based on the given time
SetPoint_t *getSetPointFromElapsedTime(double delta); // Returns the expected setpoint based on the given time
SetPoint_t *getSetPointFromElapsedTime(time_t delta); // Returns the expected setpoint based on the given time

// Endpoints
bool getState(JsonVariant &root);
bool getActiveProfile(JsonVariant &root);
bool getStoredProfiles(JsonVariant &root); // The request is passed through to provide the possibility to create a chucked response
bool getTemperatureHistory(JsonVariant &root, int dataPointsLimit);
bool performAction(JsonVariant &root, const String &action, const String &value);
bool updatePidTuning(JsonVariantConst root);
bool getPidTuning(JsonVariant &root);

String getStateAsString(State_t inState);
String getHeatingStageAsString(HeatingStage_t inStage);

///////////////////////////////////
//   Realtime task declarations  //
///////////////////////////////////
Task temperatureTask(1, -1, &readTemperatures);
Task processingTask(1000, -1, &process);
Task outputTask(1, -1, &output);
Task safetyTask(1, -1, &safetyLoop);
Task wifiTask(1, -1, &wifiLoop);

Scheduler runner;

// https://arduinojson.org/v6/how-to/merge-json-objects/
void merge_json(JsonVariant dst, JsonVariantConst src)
{
    if (src.is<JsonObject>())
    {
        for (auto kvp : src.as<JsonObject>())
        {
            merge_json(dst.getOrAddMember(kvp.key()), kvp.value());
        }
    }
    else
    {
        dst.set(src);
    }
}

void register_endpoints()
{
    ESP_LOGI("", "Registering endpoints");

    // The functions below are used during emergency mode
    server.onNotFound([](AsyncWebServerRequest *request)
                      { request->send(404, "text/plain", "Not found"); });

    server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  AsyncJsonResponse *response = new AsyncJsonResponse(false, 1024);
                  response->getRoot()["version"] = String(VERSION);
                  response->getRoot()["timestamp"] = String(BUILD_TIMESTAMP);

                  response->setLength();
                  response->setCode(200);

                  request->send(response);
              });

    server.on(
        "/update", HTTP_POST, [](AsyncWebServerRequest *request)
        { request->send(200, !Update.hasError() ? "OK" : "FAIL"); },
        [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final)
        {
            if (!index)
            {
                Update.runAsync(true);

                fsUpdate = request->getParam("SPIFFS") != nullptr;

                Serial.printf(fsUpdate ? "FS Update Start: %s\n" : "Update Start: %s\n", filename.c_str());

                size_t updateSize = fsUpdate ? ((size_t)&_FS_end - (size_t)&_FS_start) : ((ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000);
                fsUpdate = request->getParam("SPIFFS") != nullptr;

                if ((updateStarted = Update.begin(updateSize, fsUpdate ? U_FS : U_FLASH), !updateStarted))
                {
                    Update.printError(Serial);
                }
#ifndef ATOMIC_FS_UPDATE
                // Write to the RTC that the update has started
                system_rtc_mem_write(RTC_MEMORY_UPDATE_SIZE, &updateSize, sizeof(size_t));
#endif
            }
            if (!Update.hasError())
            {
                if (Update.write(data, len) != len)
                {
                    Update.printError(Serial);
                }
            }
            if (final)
            {
                if (Update.end(true))
                {
// Write to the RTC that the update has started
#ifndef ATOMIC_FS_UPDATE
                    size_t val = 0;
                    system_rtc_mem_write(RTC_MEMORY_UPDATE_SIZE, &val, sizeof(size_t));
#endif
                    Serial.printf("Update Success: %uB\n", index + len);
                }
                else
                {
                    Update.printError(Serial);
                }
            }
        });

    server.on("/state", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  // https://github.com/me-no-dev/ESPAsyncWebServer#arduinojson-advanced-response
                  AsyncJsonResponse *response = new AsyncJsonResponse(false, 2048);
                  bool ok = getState(response->getRoot());
                  response->setLength();
                  response->setCode(ok ? 200 : 500);
                  request->send(response);

                  // Only restart when the update is finished and that request has been sent out
                  if (updateStarted && Update.isFinished() && !Update.hasError())
                  {
                      // This will make sure that a final request can be made that should return 100% progress on the update
                      if (!shouldReboot)
                      {
                          shouldReboot = true;
                      }
                      else
                      {
                          delay(100);
                          ESP.restart();
                          ESP.restart();
                      }
                  }
              });

#ifndef ATOMIC_FS_UPDATE
    size_t updateSize = SIZE_MAX;

    system_rtc_mem_read(RTC_MEMORY_UPDATE_SIZE, &updateSize, sizeof(size_t));

    if (updateSize != SIZE_MAX && updateSize != 0)
    {
        // Emergency mode!
        server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
                  { request->send_P(200, "text/html", emergency_page); });
    }
    else
    {
#endif
        // Route for root / web page
        server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
                  { request->send(LittleFS, "/index.html"); });
        server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request)
                  {
                      int dataPoints = (request->getParam("DataPoints") != NULL) ? atoi(request->getParam("DataPoints")->value().c_str()) : 1;
                      AsyncJsonResponse *response = new AsyncJsonResponse(false, MAX_JSON_SIZE);
                      bool ok = getTemperatureHistory(response->getRoot(), dataPoints);
                      response->setLength();
                      response->setCode(ok ? 200 : 500);
                      request->send(response);
                  });
        server.on("/active_profile", HTTP_GET, [](AsyncWebServerRequest *request)
                  {
                      AsyncJsonResponse *response = new AsyncJsonResponse(false, MAX_JSON_SIZE);
                      bool ok = getActiveProfile(response->getRoot());
                      response->setLength();
                      response->setCode(ok ? 200 : 500);
                      request->send(response);
                  });

        server.on("/action", HTTP_GET, [](AsyncWebServerRequest *request)
                  {
                      AsyncJsonResponse *response = new AsyncJsonResponse(false, MAX_JSON_SIZE);
                      bool ok = performAction(response->getRoot(), request->getParam("action")->value(), ((request->getParam("value") != NULL) ? request->getParam("value")->value() : String("")));
                      response->setLength();
                      response->setCode(ok ? 200 : 500);
                      request->send(response);
                  });

        server.on("/stored_profiles", HTTP_GET, [](AsyncWebServerRequest *request)
                  {
                      AsyncJsonResponse *response = new AsyncJsonResponse(false, MAX_JSON_SIZE * 2);
                      bool ok = getStoredProfiles(response->getRoot());
                      response->setLength();
                      response->setCode(ok ? 200 : 500);
                      request->send(response);
                  });

        server.onRequestBody([](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
                             {
                                 if (request->url() == "/pid" && (request->method() == HTTP_POST || request->method() == HTTP_PUT))
                                 {
                                     DynamicJsonDocument root(512);

                                     std::unique_ptr<char[]> buffer(new char[len]);
                                     memcpy(buffer.get(), data, len);

                                     DeserializationError errorCode = deserializeJson(root, buffer.get());
                                     bool ok = (errorCode == DeserializationError::Code::Ok);

                                     root.shrinkToFit();

                                     if (ok)
                                     {
                                         ok = updatePidTuning(root.as<JsonVariantConst>());

                                         if (ok)
                                         {
                                             AsyncJsonResponse *response = new AsyncJsonResponse(false, 512);
                                             getPidTuning(response->getRoot());

                                             response->setLength();
                                             response->setCode(200);
                                             request->send(response);

                                             if (response != nullptr)
                                             {
                                                 delete response;
                                             }
                                         }
                                     }
                                     else
                                     {
                                         ESP_LOGE("", "Invalid input %s", errorCode.c_str());
                                     }

                                     request->send(404, "Invalid request");
                                 }
                             });

        server.on("/pid", HTTP_GET, [](AsyncWebServerRequest *request)
                  {
                      AsyncJsonResponse *response = new AsyncJsonResponse(false, 512);
                      bool ok = getPidTuning(response->getRoot());

                      response->setLength();
                      response->setCode(ok ? 200 : 500);
                      request->send(response);
                  });

        // File upload handler can be found here
        // https://github.com/me-no-dev/ESPAsyncWebServer/issues/542#issuecomment-573445113
        server.on(
            "/upload", HTTP_POST, [](AsyncWebServerRequest *request)
            {
                if (request->_tempObject)
                {
                    AsyncJsonResponse *response = new AsyncJsonResponse(false, MAX_JSON_SIZE * 3);

                    *((bool *)request->_tempObject) = getStoredProfiles(response->getRoot());

                    response->setLength();
                    response->setCode(*((bool *)request->_tempObject) ? 200 : 500);
                    request->send(response);

                    // The tempObject gets deleted automatically
                }
            },
            [](AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final)
            {
                // File upload handler
                if (len)
                {
                    FSInfo fsInfo;
                    bool gotInfo = false;
                    if ((gotInfo = LittleFS.info(fsInfo), gotInfo) && (fsInfo.totalBytes - fsInfo.usedBytes) > request->contentLength())
                    {
                        bool ok = false;

                        if (!index)
                        {
                            ESP_LOGD("", "Creating file");
                            // Multipart request
                            request->_tempFile = LittleFS.open(String(STORED_PROFILES_PATH) + "/temp", "w+");
                            request->_tempObject = new bool(ok);
                        }

                        // Goto specified position
                        request->_tempFile.seek(index);

                        size_t writtenBytes = request->_tempFile.write(data, len);

                        ok = (writtenBytes == len);
                        if (!ok)
                            ESP_LOGE("", "Written %d bytes of the %d incomming bytes", writtenBytes, len);

                        *((bool *)request->_tempObject) = ok;
                    }
                    else if (gotInfo)
                    {
                        request->send(500, "text/plain", "Not enough space on LittleFS");
                    }
                }
                else if (!len && !final)
                {
                    request->send(500, "text/plain", "No size send and not the final request");
                }

                if (final)
                {
                    ESP_LOGD("", "Final request, closing file");

                    request->_tempFile.flush(); // Make sure that it gets written
                    request->_tempFile.seek(0); // Move back to the start of the file

                    size_t size = request->_tempFile.size();

                    DynamicJsonDocument root(MAX_JSON_SIZE);
                    DeserializationError deserializationCode;

                    // len might not be populated anymore
                    *((bool *)request->_tempObject) = size == (index + len);

                    if (!*((bool *)request->_tempObject))
                        ESP_LOGE("", "Transfered sizes do not match %d vs %d", request->_tempFile.size(), (index + len));

                    ESP_LOGD("", "Parsing JSON..");

                    // Allocate a buffer to store contents of the file.
                    ReadBufferingStream bufferedFile(request->_tempFile, request->_tempFile.size());

                    // Only deserialize if lengths are matching
                    if (*((bool *)request->_tempObject) && (*((bool *)request->_tempObject) = (deserializationCode = deserializeJson(root, bufferedFile)) == DeserializationError::Code::Ok, *((bool *)request->_tempObject)))
                    {
                        root.shrinkToFit();

                        if (root.containsKey(MemberNames::kProfileName) && root.containsKey(MemberNames::kProfileDuration) &&
                            root.containsKey(MemberNames::kSetPoints) && root.getMember(MemberNames::kSetPoints).as<JsonArray>().size() != 0)
                        {
                            ESP_LOGD("", "Uploaded file is a valid JSON file, renaming");

                            String fileName = getProfileFileName(root[MemberNames::kProfileName]);

                            // When you specify a full path and the subdirs don't exist yet, they will be created automatically.
                            // Don't allow to upload to / since that might overwrite the index.html or certs
                            LittleFS.gc();
                            request->_tempFile.flush();

                            // Make sure we get the actual result not a stored one
                            *((bool *)request->_tempObject) = false;
                            // Renaming somehow corrupts the file
                            if ((*((bool *)request->_tempObject) = LittleFS.rename(String(STORED_PROFILES_PATH) + "/temp", String(STORED_PROFILES_PATH) + "/" + fileName), *((bool *)request->_tempObject)))
                            {
                                ESP_LOGD("", "Uploaded file has been renamed to %s", String(String(STORED_PROFILES_PATH) + "/" + fileName).c_str());
                                request->_tempFile = LittleFS.open(String(STORED_PROFILES_PATH) + "/" + fileName, "r");
                            }
                        }
                        else if (!root.getMember(MemberNames::kSetPoints).isNull() && !root.getMember(MemberNames::kSetPoints).as<JsonArray>().size() == 0)
                        {
                            request->send(403, "text/plain", "No setpoints are found");
                        }
                        else
                        {
                            request->send(403, "text/plain", "Invalid json");
                        }
                    }
                    else if (deserializationCode != DeserializationError::Code::Ok)
                    {
                        ESP_LOGD("", String(String("Deserialization code: ") + String(deserializationCode.c_str()) + String("\nProfile length: ") + String(request->_tempFile.size()) + String("\nFree heap:\n") + String(ESP.getFreeHeap())).c_str());
                        request->send(403, "text/plain", String(String("Deserialization code: ") + String(deserializationCode.c_str()) + String("\nProfile length: ") + String(request->_tempFile.size()) + String("\nFree heap:\n") + String(ESP.getFreeHeap())));
                    }
                    else if (!*((bool *)request->_tempObject))
                    {
                        ESP_LOGD("", String(String("Length not matching: ") + String(request->_tempFile.size() + String(" vs ") + String(len))).c_str());
                        request->send(403, "text/plain", String(String("Length not matching: ") + String(request->_tempFile.size() + String(" vs ") + String(len))));
                    }

                    request->_tempFile.close();
                }

                if (!*((bool *)request->_tempObject))
                {
                    request->_tempFile.close();
                    // Remove the temporary file
                    LittleFS.remove(String(STORED_PROFILES_PATH) + "/temp");
                }
            });
#ifndef ATOMIC_FS_UPDATE
    }
#endif
}

bool initialized = false;

void setup()
{
    Serial.begin(115200);

    log_setserial(&Serial);
    log_setquiet(0);

    ESP_LOGV("", "Hello! Booting..");
    Serial.flush();

    pinMode(SSR_PIN, OUTPUT);
    digitalWrite(SSR_PIN, LOW);

    firFilter1.setFilterCoeffs((double *)filter_taps);
    firFilter2.setFilterCoeffs((double *)filter_taps);

    ntc1.begin();
    ntc2.begin();

    // Set the default set point, which is idle
    currentFrame.currentSetPoint = (SetPoint_t *)&idleSetPoint;

    pid.SetOutputLimits(PID_MIN_WINDOW_SIZE, PID_WINDOW_SIZE);
    pid.SetMode(AUTOMATIC);
    pid.SetSampleTime((int)outputTask.getInterval());

    Update.clearError();

    if (!LittleFS.begin())
    {
        ESP_LOGE("", "Failed to start LittleFS!");
        while (true)
        {
            Serial.flush();
        }
    }

    PID_Tunings_t eepromTuning;

    EEPROM.begin(512);
    EEPROM.get(EEPROM_PID_ADDR, eepromTuning);

    if (eepromTuning.magicValue != EEPROM_PID_MAGIC_VAL)
    {
        ESP_LOGV("", "Creating stock pid config file..");
        Serial.flush();

        // Just write the pidTunings variable here, that will contain the default tunings
        EEPROM.put(EEPROM_PID_ADDR, pidTunings);
        EEPROM.commit();
    }
    else
    {
        pidTunings = eepromTuning;
    }

    environment.setOnConnectionCallback([](HomeEnvironment *environment) {

    });

    environment.setDebug(false);

    initialized = environment.initialize();

    if (initialized)
    {
        ESP_LOGV("", "Connected! IP Address: %s", WiFi.localIP().toString().c_str());

        // Start server
        server.begin();
        register_endpoints();
    }

    ESP_LOGV("", "Booted..");
    Serial.flush();

    // Initialize the scheduler
    runner.init();

    // Add the tasks to the scheduler
    runner.addTask(temperatureTask);
    runner.addTask(processingTask);
    runner.addTask(outputTask);
    runner.addTask(wifiTask);

    // Enable the tasks
    temperatureTask.enable();
    processingTask.enable();
    outputTask.enable();
    wifiTask.enable();

#if SAFETIES_ENABLED
    runner.addTask(safetyTask);
    safetyTask.enable();
#endif
}

void loop()
{
    // Execute the scheduler
    runner.execute();

    Serial.flush();
}

void process()
{
    // Preheating doesn't set the startTime, so this if statement is not entered during the preheating stage
    if (makeDataFrame() && startTime != 0) // Do allow a 'currentFrame' to be generated to display the current temperature
    {
        // Turn off the safeties for now
        if (currentState == ERROR)
        {
            ESP_LOGE("", "SAFETY ERROR!! DISABLING!!");
            stop();
        }

        // Check if the state is still running
        if (currentState == ACTIVE)
        {
            // TODO This is not working
            // Check if profile is finished
            if (activeProfile != NULL && difftime(currentFrame.time, startTime) >= activeProfile->totalDuration)
            {
                finish();
            }

            storeDataFrame(); // Finally store the data frame in the history array
        }
    }

    // This to prevent rare cases where starTime is actually 0 but the pin has not been switched off
    if (currentState == STOPPED)
    {
        // Turn off the SSR
        digitalWrite(SSR_PIN, 0);
    }
}

/*
 * Task function
*/
void wifiLoop()
{
    environment.loop();
}

/*
 * Task function
*/
void safetyLoop()
{
    if (!checkSafeties())
    {
        // Set the state
        setState(ERROR);

        // Abort
        stop();
    }
}

bool makeDataFrame()
{
    // Non-blocking delay
    if ((millis() - dataFrameTimestamp) > FRAME_INTERVAL)
    {
        dataFrameTimestamp = millis(); // Assign the timestamps
        currentFrame.time = time(nullptr);
        if (autoTuning)
        {
            currentFrame.currentSetPoint = (SetPoint_t *)&autoTuneSetPoint;
        }
        else
        {
            currentFrame.currentSetPoint = getSetPointFromTime(currentFrame.time);
        }
        currentFrame.valid = true;
        moveFramesAndcalculateDeltas();

        return true;
    }

    return false;
}

/*
 * Task function
*/
void readTemperatures()
{
    // SPI things
    //currentFrame.NTC1 = firFilter1.processReading(ntc1.readCelsius());
    //currentFrame.NTC2 = firFilter2.processReading(ntc2.readCelsius());
    currentFrame.NTC1 = ntc1.readCelsius();
    currentFrame.NTC2 = ntc2.readCelsius();
}

void moveFramesAndcalculateDeltas()
{
    // Resize will pop the last item off the vector, if needed.
    frameHistory.resize(FRAME_HISTORY_SIZE);
}

void storeDataFrame()
{
    // Don't put in the currentFrame directly, otherwhise it will be passed on by reference, which is what we don't want
    currentFrame.prev = &frameHistory.at(0);
    frameHistory.insert(frameHistory.begin(), currentFrame);
    currentFrame.reset();
}

bool checkSafeties()
{
    // Check if the requested state is to be expected
    if (frameHistory[1].valid)
    {
        bool ok = false;

        if (currentState == IDLE && currentFrame.prev != nullptr && (ok = (((DataFrame_t *)currentFrame.prev)->NTC1 - currentFrame.NTC1) > 0 && (((DataFrame_t *)currentFrame.prev)->NTC2 - currentFrame.NTC2) > 0), ok)
            errorMessage = "The temperature increased to much whilest the oven was idle.";

        double averageTemp = (currentFrame.NTC1 + currentFrame.NTC2) / 2;

        if (averageTemp > 260)
        {
            ok = false;
            errorMessage = "Temperature reached 260C or above";
        }

        if (!ok)
        {
            ok = (pidOut > (PID_WINDOW_SIZE / 2) && currentFrame.prev != nullptr && (((DataFrame_t *)currentFrame.prev)->NTC1 - currentFrame.NTC1) > 0 && (((DataFrame_t *)currentFrame.prev)->NTC2 - currentFrame.NTC2) > 0);

            if (ok)
                errorMessage = "The temperature did not increase at all with half the power output.";
        }

        if (currentState == ACTIVE && !ok)
        {
            bool setPointInvalid = false;
            if (currentFrame.currentSetPoint != NULL && currentFrame.currentSetPoint->prev != NULL)
            {
                setPointInvalid = ((averageTemp - 10) < ((SetPoint_t *)currentFrame.currentSetPoint->prev)->temperature) || ((averageTemp + 10) < ((SetPoint_t *)currentFrame.currentSetPoint->prev)->temperature);
            }

            // Only check this condition when the profile has been running for 20 seconds
            ok = (getProfileTimeElapsed() > 20 && getTimeRemainingForCurrentSetPoint() <= 5) && setPointInvalid;

            if (ok)
                errorMessage = "The target setpoint (+-10C) was not reached 5 seconds before the next setpoint, during heating";
        }

        if (currentState == ACTIVE && !ok)
        {
            // Only check this condition when the profile has been running for 20 seconds
            ok = (getProfileTimeElapsed() > 200) && (((averageTemp - 10) < pidSetPoint) && ((averageTemp + 10) < pidSetPoint));

            if (ok)
                errorMessage = String("The intermediate pid setpoint ") + String(pidSetPoint) + String("(+-10C) was not reached after 200 seconds, during heating");
        }

        // Cooling warnings below
        if (currentState == ACTIVE && !ok)
        {
            bool setPointInvalid = false;
            if (currentFrame.currentSetPoint != NULL && currentFrame.currentSetPoint->prev != NULL)
            {
                setPointInvalid = ((averageTemp - 10) > ((SetPoint_t *)currentFrame.currentSetPoint->prev)->temperature) || ((averageTemp + 10) > ((SetPoint_t *)currentFrame.currentSetPoint->prev)->temperature);
            }

            // Only check this condition when the profile has been running for 20 seconds
            ok = (getProfileTimeElapsed() > 20 && getTimeRemainingForCurrentSetPoint() <= 5) && setPointInvalid;

            if (ok)
                errorMessage = "The target setpoint (+-10C) was not reached 5 seconds before the next setpoint, during cooling";
        }

        if (!ok)
        {
            // Only check this condition when the profile has been running for 20 seconds
            ok = (getProfileTimeElapsed() > 20) && (((averageTemp - 10) > pidSetPoint) && ((averageTemp + 10) > pidSetPoint));

            if (ok)
                errorMessage = String("The intermediate pid setpoint ") + String(pidSetPoint) + String("(+-10C) was not reached after 20 seconds, during cooling");
        }

        return !ok;
    }
    return true;
}

bool getTemperatureHistory(JsonVariant &root, int dataPointsLimit)
{
    JsonArray points = root.createNestedArray("Points");

    // Only return points when actually running
    if (startTime == 0)
    {
        return false;
    }

    if (dataPointsLimit < 0)
    {
        // All data points
        dataPointsLimit = FRAME_HISTORY_SIZE;
    }
    else if (dataPointsLimit > 0) // Creating the array only makes sense when the requested points are above 0
    {
        // The values 1 and greater than 1 will be handled automatically
        // So is the out of bounds protection.
        for (int i = 0; i < FRAME_HISTORY_SIZE && i < dataPointsLimit; i++)
        {
            if (frameHistory[i].valid) // Check if there is an actual frame
            {
                JsonObject point = points.createNestedObject();
                point["t"] = frameHistory[i].time != 0 ? difftime(frameHistory[i].time, startTime) : 0.0;
                point["temp_ntc1"] = frameHistory[i].NTC1;
                point["temp_ntc2"] = frameHistory[i].NTC2;
            }
        }
    }

    return true;
}

String getProfileFileName(const String &profileName)
{
    String fileName(profileName);
    fileName.replace(" ", "_");
    fileName = fileName.substring(0, LFS_NAME_MAX - 1 - (String(STORED_PROFILES_PATH) + "/" + ".json").length()); // LFS_NAME_MAX is equal to 32 chars and one is reserved for the '\0', so subtract it

    // Add the .json extension
    if (!fileName.endsWith(".json"))
        fileName.concat(".json");

    return fileName;
}

bool getActiveProfile(JsonVariant &root)
{
    root[MemberNames::kProfileDuration] = (activeProfile != NULL) ? activeProfile->totalDuration : 0;
    root[MemberNames::kProfileName] = (activeProfile != NULL) ? activeProfile->name : "";

    // The values 1 and greater than 1 will be handled automatically
    // So is the out of bounds protection.
    JsonArray points = root.createNestedArray("Points");

    if (activeProfile != NULL && activeProfile->setPoints != NULL && activeProfile->setPointCount > 0)
    {
        double totalDuration = 0.0;

        for (size_t i = 0; i < activeProfile->setPointCount && activeProfile->setPointCount < MAX_SET_POINTS; i++)
        {
            JsonObject point = points.createNestedObject();
            point[MemberNames::kSetPointDuration] = (totalDuration += activeProfile->setPoints[i].duration);
            point[MemberNames::kTemperature] = activeProfile->setPoints[i].temperature;
        }
    }
    return true;
}

String getStateAsString(State_t inState)
{
    switch (inState)
    {
    case ERROR:
    {
        return "ERROR";
    }

    case IDLE:
    {
        return "Idle";
    }

    case ACTIVE:
    {
        return "Active";
    }

    case FINISHED:
    {
        return "Finished";
    }

    case STOPPED:
    {
        return "Stopped";
    }

    case AUTOTUNE:
    {
        return "Autotuning";
    }

    default:
    {
        return "UNKNOWN";
    }
    }
}

String getHeatingStageAsString(HeatingStage_t inStage)
{
    switch (currentHeatingStage)
    {
    case WAITING:
    {
        return "Waiting";
    }

    case PREHEAT:
    {
        return "Preheat";
    }

    case PROFILE:
    {
        return "Profile";
    }

    case COOLDOWN:
    {
        return "Cooldown";
    }

    case AUTOTUNE:
    {
        return "Autotune";
    }

    default:
    {
        return "UNKNOWN";
    }
    }
}

bool getStateAsObject(JsonObject &state)
{
    char timeBuf[30], buf[30], bufStopped[30];
    memset(timeBuf, 0, 30);
    memset(buf, 0, 30);
    memset(bufStopped, 0, 30);

    if (startTime != 0)
        strftime(buf, 30, "%T", localtime(&startTime));

    if (stopTime != 0)
        strftime(bufStopped, 30, "%T", localtime(&stopTime));

    time_t t = time(nullptr);
    strftime(timeBuf, 30, "%T", localtime(&t));

    state["time"] = timeBuf;
    state["state"] = getStateAsString(currentState);
    state["stage"] = getHeatingStageAsString(currentHeatingStage);
    state["error"] = errorMessage;
    state[MemberNames::kTemperature] = (currentFrame.NTC1 + currentFrame.NTC2) / 2;
    state[MemberNames::kTemperature + String("1")] = currentFrame.NTC1;
    state[MemberNames::kTemperature + String("2")] = currentFrame.NTC2;
    state[MemberNames::kProfileDuration] = (activeProfile != NULL) ? activeProfile->totalDuration : 0;
    state[MemberNames::kProfileName] = (activeProfile != NULL) ? activeProfile->name : "";
    state["time_started"] = buf;
    state["time_stopped"] = bufStopped;
    state["free_heap"] = ESP.getFreeHeap();
    state["pid_tuning"] = pidTuning;

    if (currentState == ACTIVE || currentState == AUTOTUNING)
    {
        state["pid_output"] = pidOut;
    }

    if (currentState == ACTIVE)
    {
        // Only send this if the state is active
        state["time_elapsed"] = getProfileTimeElapsed();
        state["time_remaining"] = getProfileTimeRemaining();
        state["target_pid_set_point"] = pidSetPoint;
        state["set_point_time_remaining"] = getTimeRemainingForCurrentSetPoint();
        state["set_point_target"] = currentFrame.currentSetPoint != NULL ? currentFrame.currentSetPoint->temperature : 0.0;
        state["prev_set_point_target"] = currentFrame.currentSetPoint != NULL ? (currentFrame.currentSetPoint->prev != NULL ? ((SetPoint_t *)currentFrame.currentSetPoint->prev)->temperature : 0) : 0;
    }

    // Check if an update is in progress or is finished
    if (updateStarted && (Update.isRunning() || Update.isFinished()))
    {
        state["update_progress"] = Update.isFinished() ? 100 : (Update.progress() * 100) / Update.size();
        state["update_type"] = fsUpdate ? "FS" : "Flash";
    }

    return true;
}

bool getState(JsonVariant &root)
{
    JsonObject state = root.createNestedObject("state");
    getStateAsObject(state);
    return true;
}

bool setState(const State_t &state)
{
    // TODO
    // This is the place to perform checks if everything is ready to change states
    currentState = state;

    if (state == State::FINISHED)
    {
        // Set the pid output to zero and output once more to actually turn off the SSR
        pidOut = 0;
        output();
    }

    return true;
}

void output()
{
    // Get the average temperature
    pidIn = currentFrame.NTC1 + currentFrame.NTC2;
    pidIn /= 2.0;

    // Check if the state is still running, this if statement will be entered during preheating
    if ((currentState == ACTIVE || currentState == AUTOTUNING) && currentState != FINISHED)
    {
        if (currentHeatingStage == HeatingStage::PREHEAT)
        {
            // First setpoint, we already know that the setPoints array is valid, or else it wouldn't even get in the PREHEAT stage
            SetPoint_t *firstSetPoint = &activeProfile->setPoints[0];

            // Set the setpoint
            pidSetPoint = firstSetPoint->temperature;

            bool ok = (((currentFrame.NTC1 + currentFrame.NTC2) / 2) > (firstSetPoint->temperature - 10.0)) && (difftime(time(nullptr), preheatStart) >= (PREHEAT_TIME / 1000));

            if (ok)
            {
                // Calling start again will start the profile
                start();
            }
            else if (difftime(time(nullptr), preheatStart) >= (PREHEAT_TIME / 1000) && !ok)
            {
                errorMessage = "Preheating failed. Temperature not reached. (Diff=" + String(((currentFrame.NTC1 + currentFrame.NTC2) / 2) - (firstSetPoint->temperature - 4.0)) + ")";
                setState(ERROR);
                stop();
            }
        }
        else
        {
#ifdef INDIRECT_TEMPERATURE
            // Remove the decimals from the pidSetPoint. This might reduce the number of oscilations during heatup
            pidSetPoint = (double)(int)getSetPointValue();
#else
            // Remove the decimals from the pidSetPoint. This might reduce the number of oscilations during heatup
            pidSetPoint = (double)(int)getCurrentSetPoint()->temperature;
#endif
        }

        if (currentHeatingStage != HeatingStage::PREHEAT && pidSetPoint - ((currentFrame.NTC1 + currentFrame.NTC2) / 2) < 10 && pidSetPoint - ((currentFrame.NTC1 + currentFrame.NTC2) / 2) > 0)
        {
            setPidTuning(2);
        }
        else if (currentHeatingStage != HeatingStage::PREHEAT) // && pidSetPoint - ((currentFrame.NTC1 + currentFrame.NTC2) / 2) < 40)
        {
            setPidTuning(1);
        }
        else if (currentHeatingStage == HeatingStage::PREHEAT)
        {
            setPidTuning(0);
        }

        // Calculate the setpoint every 2 seconds
        if (pidSetPoint != -1.0 && !autoTuning)
        {
            pid.Compute();
        }
        else if (autoTuning)
        {
            // Autotune
            autoTuning = aTune.Runtime() != 0;

            if (!autoTuning)
            {
                // These are the tuning parameters
                pidTunings.pValues[pidTuning] = aTune.GetKp();
                pidTunings.iValues[pidTuning] = aTune.GetKi();
                pidTunings.dValues[pidTuning] = aTune.GetKd();

                // Write to EEPROM
                EEPROM.put(EEPROM_PID_ADDR, pidTunings);
                EEPROM.commit();

                // Change states to idle
                setState(IDLE);
                currentHeatingStage = WAITING;

                // Remove the active profile
                delete (activeProfile->setPoints);
                delete (activeProfile);

                pidOut = 0;
            }
        }

        // Output the signal
        digitalWrite(SSR_PIN, LOW);

        if ((millis() - outputWindow) < PID_WINDOW_SIZE)
        {
            if ((millis() - outputMillis) < pidOut)
            {
                digitalWrite(SSR_PIN, HIGH);
            }
        }
        else
        {
            outputMillis = millis();
            outputWindow = millis();
        }
    }
    else if (currentState == FINISHED || currentState == STOPPED || currentState == ERROR)
    {
        // Output the signal
        digitalWrite(SSR_PIN, LOW);
    }
}

bool performAction(JsonVariant &root, const String &action, const String &value)
{
    bool ok = false;

    if (action == "start")
    {
        // Start the profile
        root["success"] = (ok = start(), ok);
    }
    else if (action == "stop")
    {
        // Stop the profile
        root["success"] = (ok = stop(), ok);
    }
    else if (action == "load_profile")
    {
        // Load the profile
        root["success"] = (ok = loadProfile(value), ok);
    }
    else if (action == "download_profile")
    {
        // Return the profile to be downloaded
        return downloadProfile(root, value);
    }
    else if (action == "delete_profile")
    {
        // Load the profile
        root["success"] = (ok = deleteProfile(value), ok);
    }
    else if (action == "reset")
    {
        // Reset the thing
        root["success"] = (ok = reset(), ok);
    }
    else if (action == "autotune")
    {
        // Start autotuning
        activeProfile = new Profile_t();
        activeProfile->setPointCount = 1;
        activeProfile->setPoints = (SetPoint_t *)malloc(sizeof(SetPoint_t));
        activeProfile->setPoints[0] = autoTuneSetPoint;

        // Start autotuning
        setState(State::AUTOTUNING);
        currentHeatingStage = HeatingStage::AUTOTUNE;
        ok = autoTuning = true;
    }

    if (ok)
    {
        JsonObject state = root.createNestedObject("state");
        ok = getStateAsObject(state);
    }

    return ok;
}

bool updatePidTuning(JsonVariantConst root)
{
    if (!root.containsKey("p") && !root.containsKey("i") && !root.containsKey("d") && !root["p"].is<JsonArray>() && !root["i"].is<JsonArray>() && !root["d"].is<JsonArray>())
    {
        ESP_LOGE("", "Could not find the required keys in the JSON");
        return false;
    }

    for (uint8_t i = 0; i < 3; i++)
    {
        pidTunings.pValues[i] = root["p"].as<JsonArrayConst>().getElement(i).as<double>();
        pidTunings.iValues[i] = root["i"].as<JsonArrayConst>().getElement(i).as<double>();
        pidTunings.dValues[i] = root["d"].as<JsonArrayConst>().getElement(i).as<double>();
    }

    // Write to EEPROM
    EEPROM.put(EEPROM_PID_ADDR, pidTunings);
    EEPROM.commit();

    // Update the tunings into the actual PID controller
    setPidTuning(pidTuning);

    return true;
}

bool getPidTuning(JsonVariant &root)
{
    bool ok = true;

    JsonArray kArray = root.createNestedArray("p");
    JsonArray iArray = root.createNestedArray("i");
    JsonArray dArray = root.createNestedArray("d");

    for (uint8_t i = 0; i < 3; i++)
    {
        kArray.add<double>(pidTunings.pValues[i]);
        iArray.add<double>(pidTunings.iValues[i]);
        dArray.add<double>(pidTunings.dValues[i]);
    }

    return ok;
}

bool getStoredProfiles(JsonVariant &root)
{
    bool ok = false;
    JsonArray profiles = root.createNestedArray("Profiles");

    Dir dir = LittleFS.openDir(STORED_PROFILES_PATH);
    while (dir.next())
    {
        if (dir.fileSize())
        {
            JsonObject profile = profiles.createNestedObject();
            File file = dir.openFile("r");
            ok = getFileInfo(file, profile);

            if (!ok)
            {
                ESP_LOGE("", "Loading of data for file did not complete");
            }

            if (file.isFile())
                file.close();
        }
    }

    return ok;
}

bool loadProfile(String profileName)
{
    if (currentState == ACTIVE)
    {
        ESP_LOGD("", "Skipped loading profile whilest oven is running");
        // Don't load the profile whilest active
        return false;
    }

    bool ok = false;

    // Get the full filename
    String fileName = getProfileFileName(profileName);
    String fullPath = String(STORED_PROFILES_PATH + String("/") + fileName);

    ok = LittleFS.exists(fullPath);

    if (!ok)
    {
        ESP_LOGW("", "Requested profile file does not exist (%s)", fullPath.c_str());
        return ok;
    }

    File file = LittleFS.open(fullPath, "r");
    size_t size = file.size();

    Profile_t *profile = new Profile_t();
    ok = (profile != NULL);

    if (!ok)
    {
        ESP_LOGE("", "Failed to allocate new Profile()");
        return ok;
    }

    DynamicJsonDocument root(MAX_JSON_SIZE);
    DeserializationError errorCode;
    ReadBufferingStream bufferedFile(file, size);

    // Just to be sure, force ArduinoJson to copy the input values using (const char*)
    //if (ok && (ok = (errorCode = deserializeJson(root, (const char *)buf.get()), errorCode) == DeserializationError::Code::Ok, ok))
    if (ok && (ok = (errorCode = deserializeJson(root, bufferedFile), errorCode) == DeserializationError::Code::Ok, ok))
    {
        root.shrinkToFit();

        profile->name = String(root[MemberNames::kProfileName].as<const char *>());
        profile->totalDuration = root[MemberNames::kProfileDuration].as<double>();
        profile->setPointCount = root[MemberNames::kSetPoints].as<JsonArrayConst>().size();
        profile->setPoints = (SetPoint_t *)malloc(sizeof(SetPoint_t) * profile->setPointCount); // Allocate memory for the setpoints
        // Only continue if there is memory available
        if (ok = profile->setPoints != NULL, ok)
        {
            for (size_t i = 0; i < profile->setPointCount; i++)
            {
                profile->setPoints[i].duration = root[MemberNames::kSetPoints].as<JsonArrayConst>()[i][MemberNames::kSetPointDuration].as<double>();
                profile->setPoints[i].temperature = root[MemberNames::kSetPoints].as<JsonArrayConst>()[i][MemberNames::kSetPointTemperature].as<double>();
                profile->setPoints[i].prev = (void *)(i > 0) ? &profile->setPoints[i - 1] : NULL;
                profile->setPoints[i].next = (void *)(i < (profile->setPointCount - 1)) ? &profile->setPoints[i + 1] : NULL;
            }
        }
        else
        {
            ESP_LOGE("", "Failed to allocate setpoint memory, during loading");
        }
    }
    else
    {
        ESP_LOGE("", "Failed to load the JSON for the profile (%s)", errorCode.c_str());
    }

    // Close the file
    file.close();

    if (!ok)
    {
        // Clean up the array of setpoints
        if (profile->setPoints)
            delete (profile->setPoints);

        // Clean up the malloced profile object
        if (profile)
            delete (profile);
    }
    else
    {
        if (activeProfile != NULL && activeProfile->setPoints)
        {
            delete (activeProfile->setPoints);
            delete (activeProfile);
            activeProfile = NULL;
        }

        activeProfile = profile;
    }

    return ok;
}

bool downloadProfile(JsonVariant &root, const String &profileName)
{
    String fullPath = String(STORED_PROFILES_PATH + String("/") + getProfileFileName(profileName));
    if (!LittleFS.exists(fullPath))
    {
        return false;
    }

    File file = LittleFS.open(fullPath.c_str(), "r");
    const size_t size = file.size();

    DynamicJsonDocument temp(MAX_JSON_SIZE);

    ReadBufferingStream bufferedFile(file, size);
    DeserializationError errorCode = deserializeJson(temp, bufferedFile);

    temp.shrinkToFit();

    file.close(); // Don't forget to close the file.

    merge_json(root, temp);

    return (errorCode == DeserializationError::Code::Ok);
}

bool deleteProfile(String profileName)
{
    return LittleFS.remove(String(STORED_PROFILES_PATH + String("/") + getProfileFileName(profileName)).c_str());
}

bool getFileInfo(File file, JsonObject &output)
{
    bool ok = false;
    const size_t size = file.size();

    if (size == 0 || !file.isFile())
    {
        output[MemberNames::kError] = "File size is 0 or file is not a file";
        ESP_LOGW("", "Filename: %s (size %d)", file.fullName(), size);
        return false;
    }

    DynamicJsonDocument root(MAX_JSON_SIZE);
    DeserializationError errorCode;
    ReadBufferingStream bufferedFile(file, size);

    // The (const char*) cast is needed to force ArduinoJson to make a copy of the input, the buffer will get destroyed when this function returns
    if (ok = ((errorCode = deserializeJson(root, bufferedFile), errorCode) == DeserializationError::Code::Ok), !ok)
    {
        ESP_LOGW("", "Filename: %s (size %d), Error parsing JSON %s", file.fullName(), size, errorCode.c_str());
        output[MemberNames::kError] = errorCode.c_str();
    }

    root.shrinkToFit();

    // This function should not close the file, as it might be used after this function returns

    // For now deserializeJson(JsonVariant..) is not supported. Will be in V7 (https://github.com/bblanchon/ArduinoJson/issues/1226#issuecomment-695786965)
    if (ok)
    {
        merge_json(output, root.as<JsonObject>());
    }

    ok = output.containsKey(MemberNames::kProfileName);

    return ok;
}

bool shouldPreheat()
{
    if (activeProfile != NULL && activeProfile->setPoints != NULL && activeProfile->setPointCount > 0)
    {
        double averageTemp = (currentFrame.NTC1 + currentFrame.NTC2) / 2;

        if ((activeProfile->setPoints[0].temperature - 20) > averageTemp)
        {
            // Preheat
            ESP_LOGD("", "Should preheat: 1");
            return true;
        }
    }
    ESP_LOGD("", "Should preheat: 0");
    return false;
}

bool start()
{
    if (currentHeatingStage == HeatingStage::WAITING && shouldPreheat())
    {
        setState(ACTIVE);
        currentHeatingStage = HeatingStage::PREHEAT;
        preheatStart = time(nullptr);
        preheatStartTemp = currentFrame.NTC1 + currentFrame.NTC2;
        preheatStartTemp /= 2;
        setPidTuning(0);

        pid.SetMode(MANUAL);
        pidOut = 2000;
        // Don't return true here
        //return true;
    }
    else if (!shouldPreheat())
    {
        setState(ACTIVE);
        currentHeatingStage = HeatingStage::PROFILE;

        double averageTemp = (currentFrame.NTC1 + currentFrame.NTC2) / 2;

        if (activeProfile != nullptr && averageTemp >= (activeProfile->setPoints[0].temperature - 20))
        {
            // Calculate the entrance point, temperature wise and adjust the runtime for that
            SetPoint_t *entranceSetPoint = nullptr;
            size_t entranceIndex = 0;
            long accumulatedTime = 0;

            do
            {
                // We want to have the actual entrance a little bit earlier than exactly the target temperature.
                // Otherwhise the PID controller fully will ramp down
                if ((activeProfile->setPoints[entranceIndex].temperature) > averageTemp)
                {
                    // Entrance point found, make sure that it's not the last one
                    if (activeProfile->setPoints[entranceIndex].next != nullptr)
                    {
                        entranceSetPoint = &activeProfile->setPoints[entranceIndex];
                    }
                    else if (activeProfile->setPoints[entranceIndex].next == nullptr && activeProfile->setPoints[entranceIndex].prev != nullptr)
                    {
                        // If it is the last one, make sure to go one beyond.
                        // No need to update entranceIndex any further. It is not used
                        entranceSetPoint = (SetPoint_t *)activeProfile->setPoints[entranceIndex].prev;
                    }
                }
                else
                {
                    accumulatedTime += (long)activeProfile->setPoints[entranceIndex].duration;
                    entranceIndex++;
                }
            } while (entranceSetPoint == nullptr && entranceIndex < activeProfile->setPointCount);

            pid.SetMode(AUTOMATIC);

            startTime = time(nullptr) - accumulatedTime;
        }
        else
        {
            startTime = time(nullptr);
        }

        outputMillis = millis();
        outputWindow = millis();

        return true;
    }

    return false;
}

bool stop()
{
    // Stop it.
    stopTime = time(nullptr);
    pidSetPoint = 0.0;
    setState(STOPPED);

    return true;
}

bool finish()
{
    bool ok = stop();
    return (ok && setState(FINISHED));
}

bool reset()
{
    startTime = 0;
    stopTime = 0;
    errorMessage = ""; // Clear the error message
    //pidSetPoint = 0;

    // Changing the mode from automatic -> manual -> automatic
    // should (hopefully) reset the pid controller
    // The Initialize function at least gets called when calling SetMode
    pid.SetMode(MANUAL);
    pidOut = 0.0;
    pid.SetMode(AUTOMATIC);

    frameHistory.clear();
    frameHistory.reserve(FRAME_HISTORY_SIZE);
    frameHistory.resize(FRAME_HISTORY_SIZE);

    currentHeatingStage = HeatingStage::WAITING;

    return setState(IDLE);
}

// Returns the expected setpoint based on the elapsed time
SetPoint_t *getCurrentSetPoint()
{
    if (autoTuning)
    {
        return (SetPoint_t *)&autoTuneSetPoint;
    }

    return getSetPointFromTime(time(nullptr));
}

// Returns the expected setpoint based on the given time
SetPoint_t *getSetPointFromTime(time_t time)
{
    // Check if the oven has actually started
    if ((startTime == 0) && (time == 0))
    {
        if (activeProfile != NULL && activeProfile->setPointCount > 0 && activeProfile->setPoints != NULL)
        {
            // This will return the first item in the array
            return activeProfile->setPoints;
        }
        else
        {
            return NULL;
        }
    }
    return getSetPointFromElapsedTime((double)difftime(time, startTime));
}

// Returns the expected setpoint based on the given time
SetPoint_t *getSetPointFromElapsedTime(time_t delta)
{
    // delta is in seconds
    return getSetPointFromElapsedTime((double)delta);
}

// Returns the expected setpoint based on the given time, delta is in seconds
SetPoint_t *getSetPointFromElapsedTime(double delta)
{
    if (startTime != 0 && activeProfile != NULL && activeProfile->setPoints != NULL)
    {
        double accumulatedTime = 0;

        for (size_t i = 0; i < activeProfile->setPointCount; i++)
        {
            if (delta < (accumulatedTime += activeProfile->setPoints[i].duration))
            {
                return &activeProfile->setPoints[i];
            }
        }
    }
    return NULL;
}

// Calculates the remaining time for the current setpoint
double getTimeRemainingForCurrentSetPoint()
{
    if (activeProfile == NULL || startTime == 0)
    {
        return currentFrame.currentSetPoint != NULL ? currentFrame.currentSetPoint->duration : 0.0;
    }

    double accumulatedTime = 0.0;

    // This could be done every for loop, but it may take more time doing it every loop than the accuracy improvement of doing it only once.
    double elapsed = getProfileTimeElapsed();

    for (size_t i = 0; i < activeProfile->setPointCount; i++)
    {
        if (elapsed < (accumulatedTime += activeProfile->setPoints[i].duration))
        {
            return (accumulatedTime - elapsed);
        }
    }

    return 0.0;
}

void setPidTuning(uint8_t tuningIndex)
{
    pid.SetTunings(pidTunings.pValues[tuningIndex], pidTunings.iValues[tuningIndex], pidTunings.dValues[tuningIndex], tuningIndex < 2 ? P_ON_M : P_ON_E);
    pidTuning = tuningIndex;
}

void AutoTuneHelper(boolean start)
{
    if (start)
        ATuneModeRemember = pid.GetMode();
    else
        pid.SetMode(ATuneModeRemember);
}

void updateAutoTune()
{
    if (!autoTuning)
    {
        //Set the output to the desired starting frequency.
        pidOut = aTuneStartValue;
        aTune.SetNoiseBand(aTuneNoise);
        aTune.SetOutputStep(aTuneStep);
        aTune.SetLookbackSec((int)aTuneLookBack);
        AutoTuneHelper(true);
        autoTuning = true;
    }
    else
    { //cancel autotune
        aTune.Cancel();
        autoTuning = false;
        AutoTuneHelper(false);
    }
}

// Calculates the elapsed time for the current setpoint
double getTimeElapsedForCurrentSetPoint()
{
    if (activeProfile == NULL || startTime == 0)
    {
        return currentFrame.currentSetPoint != NULL ? currentFrame.currentSetPoint->duration : 0.0;
    }

    double accumulatedTime = 0.0;

    // This could be done every for loop, but it may take more time doing it every loop than the accuracy improvement of doing it only once.
    double elapsed = getProfileTimeElapsed();

    for (size_t i = 0; i < activeProfile->setPointCount; i++)
    {
        if (elapsed < (accumulatedTime += activeProfile->setPoints[i].duration))
        {
            return activeProfile->setPoints[i].duration - (accumulatedTime - elapsed);
        }
    }

    return 0.0;
}

// This function returns the temperature value that is used to smooth out the curve.
// This makes sure that the value increases according to the profile curve.
// Call regularly
double getSetPointValue()
{
    SetPoint_t *inSetPoint = getCurrentSetPoint();

    if (inSetPoint == NULL)
    {
        return -1.0;
    }

    double averageTemp = (currentFrame.NTC1 + currentFrame.NTC2) / 2;
    double degreesPerSecond, temperatureDelta;

    if (preheatStartTemp > inSetPoint->temperature)
    {
        // Advance one setpoint
        inSetPoint = (SetPoint_t *)inSetPoint->next;
    }

    // Let's hope it will stay increasing if we use the next setpoint instead of the current one
    if (inSetPoint->prev != NULL && inSetPoint->next != NULL && ((SetPoint_t *)inSetPoint->next)->temperature > inSetPoint->temperature)
    {
        temperatureDelta = ((SetPoint_t *)inSetPoint->next)->temperature - inSetPoint->temperature;
    }
    else
    {
        if (inSetPoint->temperature > averageTemp)
        {
            temperatureDelta = inSetPoint->temperature - averageTemp;
        }
        else
        {
            temperatureDelta = 0;
        }
    }

    double setPointTimeRemaining = getTimeRemainingForCurrentSetPoint();
    double setPointTimeElapsed = getTimeElapsedForCurrentSetPoint();

    degreesPerSecond = temperatureDelta / inSetPoint->duration; // On a perfect oven, this value should rise in a lineair fashion, temperatureRemaining will increase and the duration will be the same
    // TODO Add a check if the oven is capable of heating this quick, according to calibration
    return (int)(((SetPoint_t *)inSetPoint->prev) != NULL ? ((SetPoint_t *)inSetPoint->prev)->temperature + (degreesPerSecond * setPointTimeElapsed) : degreesPerSecond * setPointTimeElapsed);
}

// Returns the elapsed time of the current runningprofile
double getProfileTimeElapsed()
{
    if (startTime != 0)
        return difftime(time(nullptr), startTime);

    return 0.0;
}

// Returns the remaining time of the current runningprofile
double getProfileTimeRemaining()
{
    if (activeProfile == NULL)
        return 0.0;

    return abs(activeProfile->totalDuration - getProfileTimeElapsed());
}