import datetime
import os
import subprocess

Import("env")

VERSION_FILE = 'FirmwareVersion.cpp'

result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE).stdout.decode('utf-8').replace('\n','')

print("Generating details")

VERSION_CONTENTS = """
#include "FirmwareVersion.h"
String FirmwareVersion::getGitCommitSha1() {{
 return "{sha}";
}}
String FirmwareVersion::getBuildTimestamp() {{
 return "{ts}";
}}
""".format(sha = result, ts = datetime.datetime.now())

if os.environ.get('PLATFORMIO_INCLUDE_DIR') is not None:
    VERSION_FILE = os.environ.get('PLATFORMIO_INCLUDE_DIR') + os.sep + VERSION_FILE
elif os.path.exists("include"):
    VERSION_FILE = "include" + os.sep + VERSION_FILE
else:
    PROJECT_DIR = env.subst("$PROJECT_DIR")
    os.mkdir(PROJECT_DIR + os.sep + "include")
    VERSION_FILE = "include" + os.sep + VERSION_FILE

print("Updating {} with FirmwareVersion/timestamp...".format(VERSION_FILE))
with open(VERSION_FILE, 'w+') as FILE:
        FILE.write(VERSION_CONTENTS)