Import("env")

# add macro definition for code to use in projenv (visible to src/ files) 
env.Append(CPPDEFINES=[
  ("wifi_ssid", "\"SSID\"")
])

env.Append(CPPDEFINES=[
  ("wifi_password", "\"PASS\"")
])

env.Append(CPPDEFINES=[
  ("ota_password", "\"PASS\"")
])