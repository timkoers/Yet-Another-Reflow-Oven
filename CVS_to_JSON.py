import csv 
import json 
import sys

min_temperature = 50

def diff(one, two):
    return one - two

# Function to convert a CSV to JSON 
# Takes the file paths as arguments 
def make_json(csvFilePath, jsonFilePath): 
    
    decimal_accuracy = 1
    
    # create a dictionary 
    data = {} 
    
    # first get all lines from file
    with open(csvFilePath, 'r') as f:
        lines = f.readlines()

    # remove spaces and replace , by .
    lines = [line.replace(' ', '').replace(',', '.') for line in lines]

    # finally, write lines in the file
    with open(csvFilePath, 'w') as f:
        f.writelines(lines)
    
    # Open a csv reader called DictReader 
    with open(csvFilePath, encoding='utf-8') as csvf: 
        
        processed_duration = 0.0

        data["prof_name"] = csvFilePath.replace(".csv", "").replace(".txt","").replace(".\\", "")
        data["prof_dur"] = 0.0
        data["sp"] = []
        csvReader = csv.DictReader(csvf, delimiter=';') 
        
        prev_row_c_per_sec = 0.0
        prev_temperature = 0.0

        for rows in csvReader:
        
            prev_row = {}            
            if len(data["sp"]) > 0:
                prev_row = data["sp"][-1]

            if float(rows['t']) > min_temperature:
                # The d key contains the dur of the setpoint, not the total time
                rows['d'] = round(float(rows['d']) - processed_duration,decimal_accuracy)               
                rows['t'] = round(float(rows['t']), decimal_accuracy)
                
                print("Prev C/sec {0}".format(prev_row_c_per_sec))
                c_per_sec = round((rows['t'] - prev_temperature)/rows['d'],decimal_accuracy)           
                print("Curr C/sec {0}".format(c_per_sec))
                
                #if c_per_sec == prev_row_c_per_sec:
                if abs(diff(c_per_sec, prev_row_c_per_sec)) < 0.1 and rows['t'] < 170:
                    print("Found lineairty, merging points together")
                    # Merge the previous and the current point
                    prev_row['d'] += rows['d']
                    prev_row['t'] += rows['t']
                    prev_row['t'] /= 2.0; # Devide them in half
                    prev_row['d'] = round(float(prev_row['d']), decimal_accuracy)
                    prev_row['t'] = round(float(prev_row['t']), decimal_accuracy)
                else:                
                    data["sp"].append(rows)

                processed_duration = processed_duration + round(float(rows['d']),decimal_accuracy)               
                
                prev_row_c_per_sec = c_per_sec; #(rows['t'] - prev_temperature) / rows['d'];

                print("C/sec {0}".format(prev_row_c_per_sec))
                prev_temperature = rows['t']
                # Do increase the total time though
            else:
                print("Skipping row, minimum temperature not reached")

        
        # We do need to have a (close to) zero duration first setpoint, otherwise the oven will just idle
        if data['sp'][0]['d'] > 2:
            processed_duration -= data['sp'][0]['d']
            data['sp'][0]['d'] = 2
            processed_duration += data['sp'][0]['d']

        data["prof_dur"] = round(processed_duration,decimal_accuracy)
    # Open a json writer, and use the json.dumps() 
    # function to dump data 
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf: 
        jsonf.write(json.dumps(data, indent=0)) 
        
# Driver Code 

# Decide the two file paths according to your 
# computer system 

if len(sys.argv) == 1:
    print("Please provide at least the cvs file as an argument")
    print("You can use https://apps.automeris.io/wpd/ to convert a graph picture into data")
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))
    sys.exit(2)
    
csvFilePath = str(sys.argv[1])
if len(sys.argv) == 2:
    jsonFilePath = csvFilePath.replace(".csv", ".json").replace(".txt", ".json")
elif len(sys.argv) == 3:
    jsonFilePath = str(sys.argv[2])

# Call the make_json function 
make_json(csvFilePath, jsonFilePath)
